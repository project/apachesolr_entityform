INTRODUCTION
------------

The Apachesolr Entityform module allows content stored with the
EntityForm (http://drupal.org/project/entityform) to be indexed for
search in Apache Solr (http://drupal.org/project/apachesolr).

CREDITS
-------

This project was sponsored by TATA Consultancy Services (http://www.tcs.com).
